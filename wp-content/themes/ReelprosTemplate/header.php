<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php the_title(); ?> - <?php bloginfo( 'name' ); ?></title>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/javascript.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/short_bus_to_school.css" type="text/css" media="screen" />
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>



<body>
<header>
<div>
<h1><a href="<?php bloginfo('url');?>"><img class="rounded_five" src="<?php bloginfo('template_url'); ?>/images/reelpros.jpg" alt="Reel Pros" width="300" /></a></h1>
</div>
<div style="margin-top: 10px">
<nav>
<?php wp_page_menu(); ?>
</nav>
</div>
</header>

<?php 

if(is_front_page()){ ?> 
<div id="front_page_widgets">
	<div id="image_div" class="front_page_widget">
	<?php dynamic_sidebar('slider'); ?>
	</div>
</div>
<?php } ?>

<div id="main">
<article>