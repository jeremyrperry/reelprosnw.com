<?php

class detect_old_ie{

	public $ie = false;

	public $printout = '';
	
	public $ie_version = "";
	
	public $new_win_version = false;
	
	public $win_version_check = array('Windows NT 7', 'Windows NT 6');
	
	public $browsers = array(
		'chrome'=>'<a href="http://www.google.com/chrome"><img src="%%directory%%/images/chrome.png" alt="Chrome" /></a>',
		'firefox'=>'<a href="http://www.mozilla.org"><img src="%%directory%%/images/firefox.png" alt="Firefox" /></a>',
		'safari'=>'<a href="http://www.apple.com/safari"><img src="%%directory%%/images/safari.png" alt="Safari" /></a>',
		'opera'=>'<a href="http://www.opera.com/download/"><img src="%%directory%%/images/opera.png" alt="Opera" /></a>',
		'ie'=>'<a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie"><img src="%%directory%%/images/ie.png" alt="Internet Explorer" /></a>',
	);
	
	private function get_directory(){
		$doc_root = explode("/", $_SERVER['DOCUMENT_ROOT']);
		$file_location = explode("/", dirname(__FILE__));
		$file_output = "";
		foreach($file_location as $f){
			if(!in_array($f, $doc_root)){
				$file_output .= "/".$f;
			}
		}
		return $file_output;
	}
	
	public function is_ie(){
		$old_win = false;
		
		for($i=1; $i<11; $i++){
			$version = 'MSIE '.$i;
			 if (isset($_SERVER['HTTP_USER_AGENT']) && 
    (strpos($_SERVER['HTTP_USER_AGENT'], $version) !== false)){
				$this->ie = true;
				$this->ie_version = $i;
				break;
			}
		}
		if($this->ie_version < 9){
			foreach($this->win_version_check as $w){
				if (isset($_SERVER['HTTP_USER_AGENT']) && 
    (strpos($_SERVER['HTTP_USER_AGENT'], $w) !== false)){
					$this->new_win_version = true;
					break;
				}
			}
		}
	}
	
	public function remove_browser($br){
		$temp_array = array();
		foreach($this->browsers as $b=>$bb){
			if($b != $br){
				$temp_array[$b] = $bb;
			}
		}
		$this->browsers = $temp_array;
	}
	
	public function standard_check(){
		$this->is_ie();
		if($this->ie){
			$this->printout = '<div class="is_ie">You are using an outdated version of Internet Explorer.';
			if($this->new_win_version){
				$this->printout .= '  It is recommended for you to update to the newest version of Internet Explorer or another standards compliant browser.';
			}
			else{
				$this->printout .= '  It is recommended for you to upgrade to another standards compliant browser.';
				$this->remove_browser('ie');
			}
			$this->printout .= '<br />';
			$file_dir = $this->get_directory();
			foreach($this->browsers as $b=>$bb){
				$this->printout .= str_replace('%%directory%%', $file_dir, $bb);
			}
			$this->printout .= '</div>';
			print $this->printout;
			
		}
	}

}
?>