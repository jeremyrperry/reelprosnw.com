<?php get_header(); ?>
<?php if(is_front_page()){ ?>
	<div id="whats_going_on">
	<h2>Where We're Fishing Now...</h2>
	<?php dynamic_sidebar('whats_going_on'); ?>
	<a href="/contact">Book a Trip!</a>
	</div>
<!--
	<script>
	$(document).ready(function(){
		if($(window).width() < 600){
			$('#whats_going_on').appendTo('.entry');
		}
	});
	</script>
-->
	<?php } ?>
<div id="pages">
 <!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="post">
	<h2><?php the_title(); ?></h2>
	<div class="entry">
		<?php the_content(); ?> 
		<?php if($post->ID == 12){
			comment_form();
		}	
		?> 
	</div>
 </div> 


<!-- closes the first div box -->
<?php if ($post->post_name == "contact"){ ?>
<script>
	var entry = $('.entry').height();
	var post = entry+102;
	$('.post').height(post);
</script>
<?php } ?>
 <?php endwhile; endif; ?>


 

 
 </div>
 
 <?php get_footer(); ?>