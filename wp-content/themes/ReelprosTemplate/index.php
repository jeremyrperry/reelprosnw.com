<?php get_header(); ?>

<div id="posts">
 <!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="post">
 
	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	 <small><?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?></small>
	<div class="entry">
		<?php the_excerpt(); ?>
		<p><a href="<?php the_permalink(); ?>">Read More...</a></p>
	</div>
 	<p class="postmetadata">Posted in <?php the_category(', '); ?></p>
 </div> <!-- closes the first div box -->
 <?php endwhile; else: ?>
 <p>Sorry, no posts matched your criteria.</p>

 <!-- REALLY stop The Loop. -->
 <?php endif; ?>
 

 
 </div>
 <?php get_footer(); ?>