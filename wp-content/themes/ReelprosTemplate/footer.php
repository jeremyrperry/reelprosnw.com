</article>

<footer>
<?php 
dynamic_sidebar('the_footer'); 
?>

<p>&copy; 2011-<?php echo date("Y");?> Reel Pros.  All Rights Reserved</p>
<p>Background photo courtesy: &copy; 2013 Kevin Devon Byers</p>
<p>This site was developed by <a href="http://www.jeremyrperry.com" target="_blank">JRP Web Solutions</a></p>
</footer>
<?php
/*
if(!is_front_page()){
	$is_ie = new detect_old_ie;
	$is_ie->standard_check();
}
*/
?>
</div>
</body>
</html>
