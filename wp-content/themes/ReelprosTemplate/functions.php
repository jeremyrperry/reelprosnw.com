<?php
include_once('code/Mobile_Detect.php');
include_once('ie_alternatives/class.detect.old.ie.php');
register_sidebar( array(
	'name' => 'Page Menu',
	'id' => 'page-menu',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => false,
	'after_title' => false
) );

add_filter( 'wp_page_menu', 'my_page_menu' );

function my_page_menu( $menu ) {
	dynamic_sidebar( 'page-menu' );
}

	register_sidebar( array(
		'name' => 'Slider',
		'id' => 'slider',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => '',
	));

register_sidebar( array(
	'name' => 'The Footer',
	'id' => 'the_footer',
	'before_widget' => '<div>',
	'after_widget' => '</div>',
	'before_title' => '',
	'after_title' => ''
) );

register_sidebar( array(
		'name' => 'What\'s Going On',
		'id' => 'whats_going_on',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));

?>